import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public show: boolean = false;
  public hide;

  testDrive() {
    this.show = !this.show;
    alert("Please Fill out the Form Below");
  }

  payToken() {
    this.show = this.hide;
    document.getElementById("button1").innerHTML = "Completed";
    (document.getElementById("button1")as HTMLButtonElement).disabled = true;
    alert("Please Pay Token Amount");
    const markers = document.getElementsByClassName("marker");
    {
      const marker = markers[1] as HTMLElement;
      marker.style.background = "#f10606";
    }
    (document.getElementById("button3")as HTMLButtonElement).disabled = false;
    document.getElementById("button3").style.color = "white";
    document.getElementById("button2").innerHTML = "Completed";
    (document.getElementById("button2")as HTMLButtonElement).disabled = true;
  }

  loanProcess() {
    alert(" HURRAY! Loan Processed ");
    const markers = document.getElementsByClassName("marker");
    {
      const marker = markers[2] as HTMLElement;
      marker.style.background = "#f10606";
    }
    (document.getElementById("button4")as HTMLButtonElement).disabled = false;
    document.getElementById("button4").style.color = "white";
    document.getElementById("button3").innerHTML = "Completed";
    (document.getElementById("button3")as HTMLButtonElement).disabled = true;
  }

  carDeliver() {
    alert("Congratulations On New Car");
    const markers = document.getElementsByClassName("marker");
    {
      const marker = markers[3] as HTMLElement;
      marker.style.background = "#f10606";
    }
    (document.getElementById("button5")as HTMLButtonElement).disabled = false;
    document.getElementById("button5").style.color = "white";
    document.getElementById("button4").innerHTML = "Completed";
    (document.getElementById("button4")as HTMLButtonElement).disabled = true;
  }

  transferProcess() {
    alert("Transfer Processed");
    const markers = document.getElementsByClassName("marker");
    {
      const marker = markers[4] as HTMLElement;
      marker.style.background = "#f10606";
    }
    document.getElementById("button5").innerHTML = "Completed";
    (document.getElementById("button5")as HTMLButtonElement).disabled = true;
  }

  onsubmit() {
    var x = document.forms["form"]["name"].value;
    var y = document.forms["form"]["email"].value;
    if (x == "" || y == "") {
      alert("All fields must be filled out");
      return false;
    }
    var value = ((<HTMLInputElement>document.getElementById("name")).value);
    var value1 = ((<HTMLInputElement>document.getElementById("color")).value)
    alert("Hello "+  value + " Your " + value1 + " Car Test Drive is Ready");
    const markers = document.getElementsByClassName("marker");
    {
      const marker = markers[0] as HTMLElement;
      marker.style.background = "#f10606";
    }
    (document.getElementById("button2")as HTMLButtonElement).disabled = false;
    document.getElementById("button2").style.color = "white";
  }

  reset() {
    (document.getElementById("button2")as HTMLButtonElement).disabled = true;
    (document.getElementById("button3")as HTMLButtonElement).disabled = true;
    (document.getElementById("button4")as HTMLButtonElement).disabled = true;
    (document.getElementById("button5")as HTMLButtonElement).disabled = true;
    document.getElementById("button2").style.color = "gray";
    document.getElementById("button3").style.color = "gray";
    document.getElementById("button4").style.color = "gray";
    document.getElementById("button5").style.color = "gray";
    document.getElementById("button1").innerHTML = "Take Test Drive";
    document.getElementById("button2").innerHTML = "Pay Token";
    document.getElementById("button3").innerHTML = "Loan Process";
    document.getElementById("button4").innerHTML = "Take Car Delivery";
    document.getElementById("button5").innerHTML = "Transfer Process";
    const markers = document.getElementsByClassName("marker");
    for (let i = 0; i < markers.length; i++) {
      const marker = markers[i] as HTMLElement;
      marker.style.background = "snow";
    }
  }
}

